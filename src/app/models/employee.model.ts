export class Employee {
    firstname;
    lastname;
    salary;

    constructor(firstname: string, lastname: string, salary: number) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary
    }
}