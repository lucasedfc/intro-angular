import { Injectable } from '@angular/core';
import { Employee } from '../models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  public employees: Employee[] = [];

  constructor() {
    this.employees = [
      new Employee('Jhon', 'Doe', 4500),
      new Employee('Tom', 'Masaki', 5300),
      new Employee('Jane', 'Tera', 4100),
      new Employee('Sara', 'Burton', 3200)
    ]
  }

  getEmployees() {
    return this.employees;
  }

  addEmployee(employee: Employee) {

    this.employees.push(employee);
  }

  getEmployee(id: number) {
    return this.employees[id];
  }

  deleteEmployee(id: number) {
    let employee = this.employees[id];

    if(employee) {
      this.employees.splice(id, 1);
    }
  }

  updateEmployee(id: number, data: Employee) {

    let employee = this.employees[id];

    if(employee) {
      employee.firstname = data.firstname;
      employee.lastname = data.lastname;
      employee.salary = data.salary;
    }
  }
}
