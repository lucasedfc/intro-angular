import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-employee-actions',
  templateUrl: './employee-actions.component.html',
  styleUrls: ['./employee-actions.component.scss']
})
export class EmployeeActionsComponent implements OnInit {

  public employeeForm: FormGroup;
  public action: string;
  public index: number;
  public employeeList: Employee[] = [];

  constructor(
    private employeeService: EmployeeService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,

    ) {
    this.employeeForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      salary: ['', Validators.required]
    });
   }

  ngOnInit(): void {
    this.employeeList = this.employeeService.getEmployees();
    this.index = this.route.snapshot.params['id'];
    let employee: Employee = this.employeeService.getEmployee(this.index);

    if(employee) {
      this.employeeForm.controls['firstname'].setValue(employee.firstname);
      this.employeeForm.controls['lastname'].setValue(employee.lastname);
      this.employeeForm.controls['salary'].setValue(employee.salary);
      this.route.queryParams.subscribe(params => {
        this.action = params['action'];
      })
    }
  }

  updateEmployee() {
    Swal.fire({
      title: 'Do you want to update this employee?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        let updateEmployee = new Employee(this.employeeForm.controls['firstname'].value,
          this.employeeForm.controls['lastname'].value,
          this.employeeForm.controls['salary'].value)
        this.employeeService.updateEmployee(+this.index, updateEmployee);
        Swal.fire('Updated!', '', 'success')
      }
      this.goTo('home');
    })
  }

  goTo(path: string) {
    this.router.navigate([path]);
  }

  deleteEmployee() {
    Swal.fire({
      title: 'Do you want to delete this employee?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.employeeService.deleteEmployee(+this.index);
        Swal.fire('Deleted!', '', 'success')
      }
      this.goTo('home');
    })
  }

}
