import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public employeeForm: FormGroup;


  employeeList: Employee[] = [];

  constructor(
    private employeeService: EmployeeService,
    private fb: FormBuilder,
    private router: Router
    ) {
    this.employeeForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      salary: ['', Validators.required]
    });
   }

  ngOnInit(): void {
    this.employeeList = this.employeeService.getEmployees();
  }

  addEmployee() {
    Swal.fire({
      title: 'Do you want to create this employee?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        let newEmployee = new Employee(
          this.employeeForm.controls['firstname'].value,
          this.employeeForm.controls['lastname'].value,
          this.employeeForm.controls['salary'].value)

        this.employeeService.addEmployee(newEmployee);
        Swal.fire('Created!', '', 'success')
        this.goTo('home');
      }
    })
  }

  goTo(path: string) {
    this.router.navigate([path]);
  }

}
