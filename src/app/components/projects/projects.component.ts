import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from 'src/app/models/employee.model';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  public lastname = ''
  public salary = 0
  public firstname = ''

  public employee: Employee[] = [];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goTo(path:string) {
    this.router.navigate([path]);
  }

  addEmployee() {

    this.goTo('home');
  }

}
