import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from 'src/app/models/employee.model';



@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  @Input() employee: Employee;
  @Input() idx: number;

  public index: number;
  public action: string;

  skills: string[] = [];



  constructor(private router: Router) { }

  ngOnInit(): void {

  }

  goTo(path: string) {
    this.router.navigate([path]);
  }


  addSkill(newSkill: string) {
    this.skills.push(newSkill);
  }


}
