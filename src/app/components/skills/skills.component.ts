import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  @Output() newSkillEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  addNewSkill(value: string) {
    this.newSkillEvent.emit(value);
  }

}
